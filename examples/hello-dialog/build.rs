extern crate sticky_desktop_build;

fn main() {
    sticky_desktop_build.prepare_application().unwrap();
}
