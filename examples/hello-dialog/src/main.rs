extern crate sticky_desktop as sticky;

use sticky::Application;
use sticky::dialog::{self, MessageIcon, MessageOk};

fn main() {
    Application::run(try_main).unwrap();
}

fn try_main() -> sticky::Result<()> {
    try!(dialog::message_dialog(None,
        MessageIcon::Information,
        "Hello test",
        "Hello, World!",
        MessageOk));
    Ok(())
}
