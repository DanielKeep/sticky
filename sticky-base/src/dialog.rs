pub trait MessageAction {
    type Result;
}

pub enum MessageIcon {
    None,
    Information,
    Question,
    Warning,
    Error,
}

pub struct MessageOk;

impl MessageAction for MessageOk {
    type Result = ();
}
