use std::fmt;
use std::io;

pub type Result<T> = ::std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
}

impl ::std::error::Error for Error {
    fn description(&self) -> &str {
        use self::Error::*;
        match *self {
            Io(ref e) => e.description(),
        }
    }
}

impl From<&'static str> for Error {
    fn from(v: &'static str) -> Error {
        Error::Io(io::Error::new(io::ErrorKind::Other, v))
    }
}

impl From<io::Error> for Error {
    fn from(v: io::Error) -> Error {
        Error::Io(v)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        use self::Error::*;
        match *self {
            Io(ref e) => e.fmt(fmt),
        }
    }
}
