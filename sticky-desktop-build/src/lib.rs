use std::io;

#[cfg(windows)]
pub fn prepare_application() -> io::Result<()> {
    extern crate wui_build;
    wui_build::guess()
}

#[cfg(not(windows))]
pub fn prepare_application() -> io::Result<()> {
    // Nothing to do.
    Ok(())
}
