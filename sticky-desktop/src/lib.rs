extern crate sticky_base;

#[cfg(feature="use-gtk3")] extern crate sticky_gtk3 as backend;
#[cfg(feature="use-win32")] extern crate sticky_win32 as backend;

pub mod application;
pub mod dialog;

pub use sticky_base::{Error, Result};

pub use self::application::Application;

#[doc(inline)] pub use backend::init;
