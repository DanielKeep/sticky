use gtk;

pub struct Application;

impl Application {
    pub fn run<F, R>(f: F) -> ::Result<R>
    where F: FnOnce() -> ::Result<R> {
        try!(gtk::init()
            .map_err(|()| ::Error::from("could not initialise GTK+")));
        f()
    }
}
