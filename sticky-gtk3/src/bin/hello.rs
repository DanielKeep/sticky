extern crate gtk;

use gtk::signal::Inhibit;
use gtk::signal::DialogSignals;
use gtk::traits::*;

fn main() {
    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }
    let window = gtk::Window::new(gtk::WindowType::Toplevel).unwrap();
    window.set_title("Hello");
    window.set_default_size(350, 70);

    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    let box_ = gtk::Box::new(gtk::Orientation::Horizontal, 10).unwrap();
    window.add(&box_);

    let lbl = gtk::Label::new("Click that over there.").unwrap();
    box_.add(&lbl);

    let btn = gtk::Button::new_with_label("Hello").unwrap();
    btn.connect_clicked({
        let window = window.clone();
        move |_| {
            let dlg = gtk::MessageDialog::new_with_markup(Some(&window), gtk::DIALOG_MODAL, gtk::MessageType::Info, gtk::ButtonsType::Ok, "Hello, World!").unwrap();
            dlg.connect_response({
                let dlg = dlg.clone();
                move |_, _| {
                    dlg.destroy();
                }
            });
            dlg.run();
        }});
    box_.add(&btn);

    window.show_all();
    gtk::main();
}
