pub use ::sticky_base::dialog::*;

use gtk;
use gtk::signal::DialogSignals;
use gtk::traits::*;
use ::Result;
use ::internal::escape_markup;

pub fn message_dialog<Action: MessageAction + ActionInternal>(
    parent: Option<()>,
    icon: MessageIcon,
    title: &str,
    body: &str,
    action: Action,
) -> Result<Action::Result> {
    let parent = parent.map(|_| unimplemented!());
    let markup = &escape_markup(body);
    let type_ = icon_to_raw(&icon);
    let buttons = action.to_raw();

    let dlg = gtk::MessageDialog::new_with_markup(parent, gtk::DIALOG_MODAL, type_, buttons, markup);
    let dlg = match dlg {
        Some(v) => v,
        None => {
            return Err(From::from("failed to create message dialog"));
        }
    };
    dlg.set_title(title);
    dlg.connect_response({
        let dlg = dlg.clone();
        move |_, _| {
            dlg.destroy();
        }
    });
    let result = dlg.run();

    let result = Action::result_from_raw(result);
    Ok(result)
}

fn icon_to_raw(icon: &MessageIcon) -> gtk::MessageType {
    use self::MessageIcon::*;
    use gtk::MessageType as mt;
    match *icon {
        None => mt::Other,
        Information => mt::Info,
        Question => mt::Question,
        Warning => mt::Warning,
        Error => mt::Error,
    }
}

use self::internal::ActionInternal;
mod internal {
    pub use ::sticky_base::dialog::*;

    use gtk;

    pub trait ActionInternal: MessageAction {
        fn result_from_raw(result: i32) -> Self::Result;
        fn to_raw(&self) -> gtk::ButtonsType;
    }

    impl ActionInternal for MessageOk {
        fn result_from_raw(_: i32) -> Self::Result {
            ()
        }

        fn to_raw(&self) -> gtk::ButtonsType {
            gtk::ButtonsType::Ok
        }
    }
}
