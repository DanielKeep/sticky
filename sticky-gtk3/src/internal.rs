use std::borrow::Cow;

pub fn escape_markup(s: &str) -> Cow<str> {
    // TODO: replace with `glib::escape_markup_text` once it exists.
    xml_escape_text(s)
}

fn xml_escape_text(s: &str) -> Cow<str> {
    if !(s.contains('<') || s.contains('>') || s.contains('&')) {
        Cow::Borrowed(s)
    } else {
        let cap = s.bytes()
            .map(|b| match b {
                b'<' => "&lt;".len(),
                b'>' => "&gt;".len(),
                b'&' => "&amp;".len(),
                _ => 1
            })
            .fold(0, |a, b| a + b);
        let mut r = String::with_capacity(cap);
        for c in s.chars() {
            match c {
                '<' => r.push_str("&lt;"),
                '>' => r.push_str("&gt;"),
                '&' => r.push_str("&amp;"),
                c => r.push(c)
            }
        }
        Cow::Owned(r)
    }
}

#[cfg(test)]
#[test]
fn test_xml_escape_text() {
    use self::xml_escape_text as xet;
    assert_eq!(xea("abcd"), "abcd");
    assert_eq!(xea("ab'cd"), "ab'cd");
    assert_eq!(xea("ab\"cd"), "ab\"cd");
    assert_eq!(xea("ab&cd"), "ab&amp;cd");
    assert_eq!(xea("a<b&c>d"), "a&lt;b&amp;c&gt;d");
}
