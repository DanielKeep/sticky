extern crate gtk;
extern crate sticky_base;

pub mod application;
pub mod dialog;

pub use sticky_base::{Error, Result};

mod internal;

pub fn init() -> Result<()> {
    gtk::init().map_err(|_| Error::from("could not initialise GTK+"))
}
