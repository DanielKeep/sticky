use wui;

pub struct Application;

impl Application {
    pub fn run<F, R>(f: F) -> ::Result<R>
    where F: FnOnce() -> ::Result<R> {
        match wui::with_com(None, f) {
            Ok(Ok(v)) => Ok(v),
            Ok(Err(err)) => Err(err),
            Err(err) => Err(::Error::from(err)),
        }
    }
}
