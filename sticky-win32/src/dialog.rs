pub use ::sticky_base::dialog::*;

use wui;
use ::Result;

pub fn message_dialog<Action: MessageAction + ActionInternal>(
    parent: Option<()>,
    icon: MessageIcon,
    title: &str,
    body: &str,
    action: Action,
) -> Result<Action::Result> {
    let wnd = parent.map(|_| unimplemented!());
    let caption = Some(title);
    let text = body;
    let icon = icon_to_raw(&icon);
    let action = action.to_raw();
    let type_ = icon | action;
    let result = try!(wui::message_box(wnd, text, caption, Some(type_)));
    let result = Action::result_from_raw(result);
    Ok(result)
}

fn icon_to_raw(icon: &MessageIcon) -> wui::MessageBoxType {
    use self::MessageIcon::*;
    use wui::message_box_type as mbt;
    match *icon {
        None => wui::MessageBoxType::empty(),
        Information => mbt::IconInformation,
        Question => mbt::IconQuestion,
        Warning => mbt::IconWarning,
        Error => mbt::IconError,
    }
}

use self::internal::ActionInternal;
mod internal {
    pub use ::sticky_base::dialog::*;

    use wui;

    pub trait ActionInternal: MessageAction {
        fn result_from_raw(result: wui::MessageBoxResult) -> Self::Result;
        fn to_raw(&self) -> wui::MessageBoxType;
    }

    impl ActionInternal for MessageOk {
        fn result_from_raw(_: wui::MessageBoxResult) -> Self::Result {
            ()
        }

        fn to_raw(&self) -> wui::MessageBoxType {
            wui::message_box_type::Ok
        }
    }
}
