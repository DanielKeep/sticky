extern crate sticky_base;
extern crate wui;

pub mod application;
pub mod button;
pub mod dialog;
pub mod error;
pub mod window;

pub use sticky_base::{Error, Result};

pub fn init() -> Result<()> {
    Ok(())
}
